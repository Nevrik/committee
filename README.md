Hello!

It's the training project "Admission committee".

To run the application you need to use JEE server (Tomcat) and database MySql.

Use query "all.sql" for creation database. In this query has commands for drop and creating database, creating tables and inserting records, creating users.

Use ".dist/Committee.war" for deploy application into the server.