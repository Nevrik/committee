DROP DATABASE IF EXISTS AdmissionCommittee;

CREATE database IF NOT EXISTS  AdmissionCommittee DEFAULT CHARACTER SET UTF8;
USE AdmissionCommittee;

CREATE TABLE IF NOT EXISTS User (
    id INTEGER PRIMARY KEY not null AUTO_INCREMENT,
    login VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(32) NOT NULL,
    role TINYINT NOT NULL default '1' CHECK (role IN (0 , 1))
);

CREATE TABLE IF NOT EXISTS Subjects (
    id INTEGER PRIMARY KEY not null AUTO_INCREMENT,
    name CHAR(255) NOT NULL UNIQUE
) DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS Faculty (
    id INTEGER PRIMARY KEY not null AUTO_INCREMENT,
    name CHAR(255) NOT NULL UNIQUE,
    recruit_plan INTEGER NOT NULL
)DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS Abiturient (
    id INTEGER PRIMARY KEY not null AUTO_INCREMENT,
    name CHAR(255) NOT NULL,
    patronomic CHAR(255) NOT NULL,
    surname CHAR(255) NOT NULL,
    diplom_rating FLOAT NOT NULL,
    faculty_id INTEGER NOT NULL,
    registrationMark boolean not null default false,
 	user_id INTEGER NOT NULL unique,
 	CONSTRAINT FOREIGN KEY (faculty_id) 
    	REFERENCES Faculty (id)
		ON UPDATE CASCADE ON DELETE CASCADE,
 	CONSTRAINT FOREIGN KEY (user_id) 
    	REFERENCES User (id)
		ON UPDATE CASCADE ON DELETE CASCADE
)DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS Rating (
    id INTEGER AUTO_INCREMENT not null UNIQUE,
    subject_id INTEGER NOT NULL,
    abiturient_id INTEGER NOT NULL,
    mark INTEGER,
    CONSTRAINT PRIMARY KEY (subject_id, abiturient_id),
    CONSTRAINT FOREIGN KEY (subject_id)
        REFERENCES Subjects (id)
        ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (abiturient_id)
        REFERENCES Abiturient (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS SubjectsList (
    id INTEGER not null AUTO_INCREMENT UNIQUE,
    faculty_id INTEGER NOT NULL,
    subject_id INTEGER,
    CONSTRAINT PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (faculty_id)
        REFERENCES Faculty (id)
        ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (subject_id)
        REFERENCES Subjects (id)
        ON UPDATE CASCADE ON DELETE CASCADE
)DEFAULT CHARACTER SET utf8;

GRANT SELECT, INSERT, UPDATE, DELETE ON AdmissionCommittee.* 
TO 'application'@'localhost' IDENTIFIED BY 'root';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON AdmissionCommittee.* 
TO 'administrator'@'localhost' IDENTIFIED BY 'administrator';

set names utf8;

USE AdmissionCommittee;

INSERT subjects (name) 
VALUES 
	("Математика"), 
	("Физика"), 
	("Русский язык"), 
	("Белорусский язык"), 
	("Химия"),
	("Биология"),
	("История Беларуси"),
	("Белорусския литература"),
	("География"),
	("Всемирная история новейшего времени"),
	("Английский язык"),
	("Немецкий язык"),
	("Физическая культура"),
	("Творчество"),
	("Обществоведение"),
	("Русская литература");
	
INSERT faculty (name, recruit_plan) 
VALUES 
	("Биологический", 3), 
	("Математический", 3),
	("Физический", 3),
	("Филологический", 3),
	("Юридический", 3);
	
INSERT subjectslist (faculty_id, subject_id) 
VALUES
	(1, 4), 
	(1, 7), 
	(1, 8), 
	
	(2, 4), 
	(2, 7), 
	(2, 8), 
	
	(3, 6), 
	(3, 9), 
	(3, 12), 
	
	(4, 1), 
	(4, 2), 
	(4, 3), 
	(4, 4),
	
	(5, 1), 
	(5, 2), 
	(5, 3), 
	(5, 4);
	
INSERT user (id, login, password, role) 
VALUES
	(1, "a@mail.ru", "21232F297A57A5A743894A0E4A801FC3", 0),
	(2, "2@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(3, "3@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(4, "4@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(5, "5@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(6, "6@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(7, "7@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(8, "8@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(9, "9@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(10, "10@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(11, "11@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(12, "12@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(13, "13@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(14, "14@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(15, "15@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(16, "16@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(17, "17@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(18, "18@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(19, "19@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(20, "20@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(21, "21@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(22, "22@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(23, "23@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(24, "24@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(25, "25@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1),
	(26, "26@mail.ru", "EE11CBB19052E40B07AAC0CA060C23EE", 1);
	
INSERT abiturient (id, faculty_id, diplom_rating, surname, name, patronomic, registrationMark, user_id) 
VALUES 
	(2, 1, 8.1, "Веселов", "Иван", "Васильевич",true, 2),
	(3, 1, 5.5, "Мендель", "Жанна", "Петровна",true, 3),
	(4, 1, 7.4, "Хорстман", "Петр", "Иванович",true, 4),
	(5, 1, 6, "Синичкина", "Инна", "Захаровна",true, 5),
	(6, 1, 4.1, "Агеев", "Олег", "Дмитриевич",true, 6),
	(7, 2, 3.9, "Иванов", "Игорь", "Петрович",true, 7),
	(8, 2, 5.8, "Фролов", "Андрей", "Дмитриевич",true, 8),
	(9, 2, 6.3, "Симонов", "Виктор", "Андреевич",true, 9),
	(10, 2, 6.5, "Капитонова", "Жанна", "Михайловна",true, 10),
	(11, 2, 5.2, "Жильник", "Андрей", "Федорович",true, 11),
	(12, 3, 7.7, "Бобовский", "Григорий", "Азоматович",true, 12),
	(13, 3, 7.5, "Петрова", "Ольга", "Игоревна",true, 13),
	(14, 3, 7.2, "Егорова", "Виктория", "Александровна",true, 14),
	(15, 3, 8.1, "Ясиров", "Олег", "Антонович",true, 15),
	(16, 3, 7.1, "Григорьев", "Михаил", "Михайлович",true, 16),
	(17, 3, 6.3, "Линько", "Ирина", "Матвеевна",true, 17),
	(18, 4, 6.4, "Мазур", "Инесса", "Апполоновна",true, 18),
	(19, 4, 6.5, "Жиглов", "Глеб", "",true, 19),
	(20, 4, 6.5, "Шарапов", "Владимир", "",true, 20),
	(21, 4, 7.4, "Рокотов", "Ильдар", "Олегович",true, 21),
	(22, 4, 4, "Мышкин", "Митрофан", "Егорович",true, 22),
	(23, 4, 6.0, "Пунцовский", "Антон", "Дмитриевич",true, 23),
	(24, 4, 7.0, "Орлов", "Игорь", "Петрович",true, 24),
	(25, 5, 7.6, "Залесский", "Роман", "Михеевич",true, 25),
	(26, 5, 7.9, "Буханкин", "Дмитрий", "Александрович",true, 26);
	
INSERT rating (abiturient_id,subject_id, mark) 
VALUES
	(2, 4, 70),
	(2, 7, 50),
	(2, 8, 40),

	(3, 4, 30),
	(3, 7, 30),
	(3, 8, 70),
	
	(4, 4, 30),
	(4, 7, 70),
	(4, 8, 80),
	
	(5, 4, 50),
	(5, 7, 80),
	(5, 8, 80),
	
	(6, 4, 50),
	(6, 7, 70),
	(6, 8, 60),
	
	(7, 4, 70),
	(7, 7, 50),
	(7, 8, 40),

	(11, 4, 30),
	(11, 7, 30),
	(11, 8, 70),
	
	(8, 4, 30),
	(8, 7, 70),
	(8, 8, 80),
	
	(9, 4, 50),
	(9, 7, 80),
	(9, 8, 80),
	
	(10, 4, 50),
	(10, 7, 70),
	(10, 8, 60),
	
	(12, 6, 70),
	(12, 9, 50),
	(12, 12, 40),

	(13, 6, 30),
	(13, 9, 30),
	(13, 12, 70),
	
	(14, 6, 30),
	(14, 9, 70),
	(14, 12, 80),
	
	(15, 6, 50),
	(15, 9, 80),
	(15, 12, 80),
	
	(16, 6, 50),
	(16, 9, 70),
	(16, 12, 60),
	
	(17, 6, 50),
	(17, 9, 70),
	(17, 12, 60),
	
	(18, 1, 50),
	(18, 2, 80),
	(18, 3, 65),
	(18, 4, 80),
	
	(19, 1, 50),
	(19, 2, 70),
	(19, 3, 50),
	(19, 4, 60),
	
	(23, 1, 50),
	(23, 2, 75),
	(23, 3, 20),
	(23, 4, 60),
	
	(20, 1, 50),
	(20, 2, 70),
	(20, 3, 50),
	(20, 4, 60),

	(22, 1, 30),
	(22, 2, 30),
	(22, 3, 78),
	(22, 4, 65),

	(21, 1, 30),
	(21, 2, 30),
	(21, 3, 78),
	(21, 4, 65),

	(24, 1, 40),
	(24, 2, 50),
	(24, 3, 70),
	(24, 4, 18),

	(25, 1, 35),
	(25, 2, 30),
	(25, 3, 50),
	(25, 4, 60),

	(26, 1, 20),
	(26, 2, 34),
	(26, 3, 58),
	(26, 4, 35);