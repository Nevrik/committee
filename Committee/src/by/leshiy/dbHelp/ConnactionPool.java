package by.leshiy.dbHelp;

import java.sql.Connection;

/**
 * Interface for pool of connections, takes 10 connections.<br>
 * Use method getConnection() for taking a connection.<br>
 * Use method releaseConnection for free connection<br>
 * Use method closeAll for <b>closing</b> all connections<br>
 * 
 * @author Nout
 * 
 */
public interface ConnactionPool {

    /**
     * @return connection
     * @throws InterruptedException 
     */
    public Connection getConnection() throws Exception;

    /**
     * @param conn
     *            that returned to the Pool
     */
    public void releaseConnection(Connection conn);

    /**
     * close all connections in Pool
     */
    public void closeAll();
}
