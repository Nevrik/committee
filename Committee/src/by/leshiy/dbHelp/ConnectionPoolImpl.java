package by.leshiy.dbHelp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Pool of connections, takes 10 connections.<br>
 * Use methods getConnPool() and getConnection() for taking a connection.<br>
 * Use method releaseConnection for free connection<br>
 * Use method closeAll for <b>closing</b> all connections<br>
 * 
 * @author Pavel Liashchou
 * 
 */
public class ConnectionPoolImpl implements ConnactionPool {

    private String driver;
    private String db;
    private final String dbPropertyFile = "by.leshiy.property.dbConnectProperty";
    private static ConnectionPoolImpl connect;
    private final int maxConn = 10;
    private Connection[] ConnArray = new Connection[maxConn];
    private int connectInUse = 0;
    private Connection con;

    private ConnectionPoolImpl() throws Exception {
        for (int i = 0; i < maxConn; i++) {
            ConnArray[i] = setConnection();
        }
    }

    private Connection setConnection() throws Exception {
        ResourceBundle dbProperty = ResourceBundle.getBundle(dbPropertyFile);
        db = dbProperty.getString("url");
        driver = dbProperty.getString("driver");
        Properties properties = new Properties();
        properties.setProperty("user", dbProperty.getString("login"));
        properties.setProperty("password", dbProperty.getString("password"));
        properties.setProperty("useUnicode", "true");
        properties.setProperty("characterEncoding", "UTF-8");
        Class.forName(driver);
        con = DriverManager.getConnection(db, properties);
        con.setAutoCommit(false);
        return con;
    }

    static public ConnectionPoolImpl getConnPool() throws Exception {
        if (connect == null) {
            connect = new ConnectionPoolImpl();
        }
        return connect;
    }

    @Override
    synchronized public Connection getConnection() throws Exception {
        Connection conn = null;
        while (connectInUse == 10) {
            wait();
        }
        connectInUse++;
        for (int i = 0; i < maxConn; i++) {
            if (ConnArray[i] != null) {
                conn = ConnArray[i];
                ConnArray[i] = null;
                break;
            }
        }
        notify();
        return conn;
    }

    @Override
    synchronized public void releaseConnection(Connection conn) {
        connectInUse--;
        try {
            conn.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < maxConn; i++) {
            if (ConnArray[i] == null) {
                ConnArray[i] = conn;
                break;
            }
        }
        notify();
    }

    @Override
    synchronized public void closeAll() {
        for (int i = 0; i < maxConn; i++) {
            if (ConnArray[i] != null) {
                try {
                    ConnArray[i].rollback();
                    ConnArray[i].close();
                } catch (Exception e) {
                }
            }
        }
        ConnArray = null;
    }
}
