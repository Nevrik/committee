package by.leshiy.db;

/**
 * entity for table SubjectsList
 * 
 * @author Pavel Liashchou
 * 
 */
public class SubjectsList extends Entity {

    private Faculty faculty;
    private Subjects subjects;

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Subjects getSubjects() {
        return subjects;
    }

    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }

}
