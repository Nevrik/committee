package by.leshiy.db;

import java.io.Serializable;

public abstract class Entity implements Serializable {

    private int id;

    public void setId(int i) {
        id = i;
    }

    public int getId() {
        return id;
    }
}
