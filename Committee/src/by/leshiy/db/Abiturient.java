package by.leshiy.db;

import java.util.List;

/**
 * entity for table Abiturients
 * 
 * @author Pavel Liashchou
 * 
 */
public class Abiturient extends Entity {

    private String name;
    private String patronomic;
    private String surname;
    private float diplomRating;
    private Faculty faculty;
    private boolean registrationMark;
    private List<Entity> rating;
    private User user;

    public boolean isRegistrationMark() {
        return registrationMark;
    }

    public void setRegistrationMark(boolean registrationMark) {
        this.registrationMark = registrationMark;
    }

    public float getDiplomRating() {
        return diplomRating;
    }

    public void setDiplomRating(float diplomRating) {
        this.diplomRating = diplomRating;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronomic() {
        return patronomic;
    }

    public void setPatronomic(String patronomic) {
        this.patronomic = patronomic;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        String ab = surname + " " + name + " " + patronomic;
        return ab;
    }

    public List<Entity> getRating() {
        return rating;
    }

    public void setRating(List<Entity> rating) {
        this.rating = rating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
