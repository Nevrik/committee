package by.leshiy.dao;

import java.util.List;

import by.leshiy.db.Entity;

/**
 * Interface contain methods for work with data for entity Abiturient
 * 
 * @author Pavel Liashchou
 * 
 */
public interface AbiturientDao extends Dao {

    List<Entity> getByChecked();

    void setRegistrationMark(boolean newMark, int id);
}