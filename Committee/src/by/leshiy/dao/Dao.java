package by.leshiy.dao;

import java.util.List;

import by.leshiy.db.Entity;

/**
 * General methods for working with data
 * 
 * @author Pavel Liashchou
 * 
 */

public interface Dao {

    List<Entity> getAll();

    List<Entity> getById(int id);

    List<Entity> getByField(String field, int id);

    void update(Entity db);

    void add(Entity db);

    void delete(Entity db);
}
