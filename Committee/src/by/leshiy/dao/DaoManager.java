package by.leshiy.dao;

import by.leshiy.dao.Impl.DaoManagerImpl;

/**
 * transaction manager
 * 
 * @author Pavel Liashchou
 * 
 */
public interface DaoManager {

    /**
     * @param key
     *            for HashMap interface implements Dao
     * @return object of class implements key's interface from HashMap
     *         {@link DaoManagerImpl}
     */
    public Dao getDao(Class<? extends Dao> key);

    /**
     * commits the transaction
     */
    public void transactionCommit();

    /**
     * rolls back the transaction
     */
    public void transactionRollback();

    /**
     * return connection to the ConnectionPool
     */
    public void transactionClose();

}
