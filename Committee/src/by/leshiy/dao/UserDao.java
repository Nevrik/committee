package by.leshiy.dao;

import by.leshiy.db.Entity;

/**
 * @author Pavel Liashchou
 * 
 */
public interface UserDao extends Dao {

    /**
     * Execute query form
     * "SELECT id, login, password, role FROM User WHERE login = ?"
     * 
     * @param login
     *            parameter for query
     * @return list of value by SQL query, using parse(ResultSet) for parsing
     *         ResultSet to List
     */
    public Entity getByLogin(String login);

}