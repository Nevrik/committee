package by.leshiy.dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.leshiy.dao.FacultyDao;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;

/**
 * @author Pavel Liashchou
 * 
 */
public class FacultyDaoImpl extends DaoImpl implements FacultyDao {

    private final String All_COLLUMNS = "id, name, recruit_plan";
    private final String SELECT_SQL = "SELECT " + All_COLLUMNS + " FROM Faculty";
    private final String SELECT_BY_ID_SQL = "SELECT " + All_COLLUMNS + " FROM Faculty WHERE id = ?";
    private final String UPDATE_SQL = "UPDATE Faculty SET name = ?, recruit_plan = ? WHERE id = ?";
    private final String DELETE_SQL = "DELETE FROM Faculty WHERE id = ?";
    private final String ADD_SQL = "INSERT Faculty (name, recruit_plan) VALUES (?, ?)";
    private PreparedStatement ps;

    public FacultyDaoImpl(Connection connection) {
        super(connection);
        super.SELECT_SQL = SELECT_SQL;
        super.SELECT_BY_ID_SQL = SELECT_BY_ID_SQL;
        super.DELETE_SQL = DELETE_SQL;
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.DaoImpl#parse(java.sql.ResultSet)
     */
    @Override
    protected List<Entity> parse(ResultSet rs) {
        List<Entity> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Faculty facTemp = new Faculty();
                facTemp.setId(rs.getInt(1));
                facTemp.setName(rs.getString(2));
                facTemp.setRecruitPlan(rs.getInt(3));
                result.add(facTemp);
            }
        } catch (NumberFormatException | SQLException e) {
            LOG_DB_ERROR.error("Error during parsing ResultSet. " + this, e);
            LOG_INFO.error("Error during parsing ResultSet. " + this);
        }
        return result;
    }
    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.FacultyDao#add(by.leshiy.db.Db)
     */
    @Override
    public void add(Entity db) {
        try {
            ps = super.connection.prepareStatement(ADD_SQL);
            ps.setString(1, ((Faculty) db).getName());
            ps.setInt(2, ((Faculty) db).getRecruitPlan());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + ADD_SQL, e);
            LOG_INFO.error("Error execute query " + ADD_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Dao#update(by.leshiy.db.Entity)
     */
    @Override
    public void update(Entity db) {
        try {
            ps = super.connection.prepareStatement(UPDATE_SQL);
            ps.setString(1, ((Faculty) db).getName());
            ps.setInt(2, ((Faculty) db).getRecruitPlan());
            ps.setInt(3, ((Faculty) db).getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + UPDATE_SQL, e);
            LOG_INFO.error("Error execute query " + UPDATE_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }
}