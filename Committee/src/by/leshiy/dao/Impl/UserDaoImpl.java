package by.leshiy.dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.leshiy.dao.UserDao;
import by.leshiy.db.Entity;
import by.leshiy.db.Role;
import by.leshiy.db.User;

/**
 * @author Pavel Liashchou
 * 
 */
public class UserDaoImpl extends DaoImpl implements UserDao {
    private final String SELECT_SQL = "SELECT id, login, password, role FROM User";
    private final String SELECT_BY_ID_SQL = "SELECT id, login, password, role FROM User WHERE id = ?";
    private final String SELECT_BY_LOGIN_SQL = "SELECT id, login, password, role FROM User WHERE login = ?";
    private final String UPDATE_SQL = "UPDATE User SET login = ?, password = ?, role = ? WHERE id = ?";
    private final String DELETE_SQL = "DELETE FROM User WHERE id = ?";
    private final String ADD_SQL = "INSERT User (id, login, password) VALUES (?, ?, ?)";
    private PreparedStatement ps;

    public UserDaoImpl(Connection connection) {
        super(connection);
        super.SELECT_SQL = SELECT_SQL;
        super.SELECT_BY_ID_SQL = SELECT_BY_ID_SQL;
        super.UPDATE_SQL = UPDATE_SQL;
        super.DELETE_SQL = DELETE_SQL;
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.DaoImpl#parse(java.sql.ResultSet)
     */
    @Override
    protected List<Entity> parse(ResultSet rs) {
        List<Entity> result = new ArrayList<>();
        try {
            while (rs.next()) {
                User userTemp = new User();
                userTemp.setId(rs.getInt(1));
                userTemp.setLogin(rs.getString(2));
                userTemp.setPassword(rs.getString(3));
                userTemp.setRole(Role.getRole(rs.getInt(4)));
                result.add(userTemp);
            }
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error during parsing ResultSet. " + this, e);
            LOG_INFO.error("Error during parsing ResultSet. " + this);
        }

        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.UserDao#getByLogin(java.lang.String)
     */
    @Override
    public Entity getByLogin(String login) {
        List<Entity> temp = new ArrayList<>();
        try {
            ps = connection.prepareStatement(SELECT_BY_LOGIN_SQL);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            temp = parse(rs);
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + SELECT_BY_LOGIN_SQL, e);
            LOG_INFO.error("Error execute query " + SELECT_BY_LOGIN_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
        return temp.get(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.AbiturientDao#add(by.leshiy.db.Db)
     */
    @Override
    public void add(Entity db) {
        try {
            ps = super.connection.prepareStatement(ADD_SQL);
            ps.setInt(1, ((User) db).getId());
            ps.setString(2, ((User) db).getLogin());
            ps.setString(3, ((User) db).getPassword());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + ADD_SQL, e);
            LOG_INFO.error("Error execute query " + ADD_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Dao#update(by.leshiy.db.Entity)
     */
    @Override
    public void update(Entity db) {
        // TODO need or not?
    }
}
