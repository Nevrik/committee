package by.leshiy.dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.leshiy.dao.Dao;
import by.leshiy.db.Entity;

/**
 * @author Pavel Liashchou
 * 
 */
public abstract class DaoImpl implements Dao {

    protected String SELECT_SQL = "";
    protected String SELECT_BY_ID_SQL = "";
    protected String SELECT_BY_FIELD_SQL = "";
    protected String UPDATE_SQL = "";
    protected String DELETE_SQL = "";
    protected String ADD_SQL = "";
    protected Connection connection = null;
    private PreparedStatement ps = null;
    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");

    public DaoImpl(Connection connection) {
        this.connection = connection;
    }

    /**
     * Execute query form "Select * from Entity"
     * 
     * @return return all data from the table 'Entity'
     */
    @Override
    public List<Entity> getAll() {
        return getFromDb(SELECT_SQL);
    }

    /**
     * Execute query form "Select * from Entity where id=?"
     * 
     * @return all data from the table Entity by field id
     */
    @Override
    public List<Entity> getById(int id) {
        return getFromDb(SELECT_BY_ID_SQL, id);
    }

    /**
     * Execute query form "SELECT * FROM Entity WHERE field  = id"
     * 
     * @param field
     * @param id
     * @return all data from the table Entity by field, where value of the field
     *         = id
     */
    @Override
    public List<Entity> getByField(String field, int id) {
        return getFromDbByField(SELECT_BY_FIELD_SQL, id);
    }

    /**
     * Execute query form "DELETE FROM Entity WHERE id = db.getId()"
     * 
     * @param db
     *            data for insert
     */
    @Override
    public void delete(Entity db) {
        executeQuery(DELETE_SQL, db);
    }

    /**
     * @param sql
     *            query for delete data from Entity
     * @param db
     *            Entity of sql query
     */
    protected void executeQuery(String sql, Entity db) {
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, db.getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + sql, e);
            LOG_INFO.error("Error execute query " + sql);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * @param sql
     *            query
     * @param id
     *            parameter for query
     * @return list of value by SQL query, using parse(ResultSet) for parsing
     *         ResultSet to List
     */
    protected List<Entity> getFromDb(String sql, int... id) {
        List<Entity> temp = new ArrayList<>();
        try {
            ps = connection.prepareStatement(sql);
            if (id.length != 0) {
                ps.setInt(1, id[0]);
            }
            ResultSet rs = ps.executeQuery();
            temp = parse(rs);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + sql, e);
            LOG_INFO.error("Error execute query " + sql);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
        return temp;
    }

    /**
     * @param sql
     *            query
     * @param id
     *            parameter for query
     * @return list of value by SQL query, using parse(ResultSet) for parsing
     *         ResultSet to List
     */
    protected List<Entity> getFromDbByField(String sql, int id) {
        List<Entity> temp = new ArrayList<>();
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            temp = parse(rs);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + sql, e);
            LOG_INFO.error("Error execute query " + sql);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
        return temp;
    }

    /**
     * Execute query form
     * "UPDATE Entity SET EntityFields = dbFields WHERE  id = db.getId()"
     * 
     */
    public abstract void update(Entity db);

    /**
     * Execute query form "INSERT Entity (EntityFields) VALUES (dbFields)"
     * 
     * @param db
     *            data for insert
     * @throws SQLException
     */
    public abstract void add(Entity db);

    /**
     * @param rs
     *            from query getAll, getById, getFromDb, getFromDbByField
     * @return list of objects of the Entity
     */
    protected abstract List<Entity> parse(ResultSet rs);

}
