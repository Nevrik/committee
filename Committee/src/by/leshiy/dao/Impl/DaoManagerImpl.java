package by.leshiy.dao.Impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import by.leshiy.dao.AbiturientDao;
import by.leshiy.dao.Dao;
import by.leshiy.dao.DaoManager;
import by.leshiy.dao.FacultyDao;
import by.leshiy.dao.RatingDao;
import by.leshiy.dao.SubjectsDao;
import by.leshiy.dao.SubjectsListDao;
import by.leshiy.dao.UserDao;
import by.leshiy.dbHelp.ConnectionPoolImpl;

/**
 * transaction manager
 * 
 * @author Pavel Liashchou
 * 
 */
public class DaoManagerImpl implements DaoManager {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private static Map<Class<? extends Dao>, Class<? extends DaoImpl>> factoryDao = new HashMap<>();
    private Connection connection;

    protected DaoManagerImpl() {
        try {
            connection = ConnectionPoolImpl.getConnPool().getConnection();
        } catch (Exception e) {
            LOG_DB_ERROR.fatal("Error connecting to the data store. Application is stopped.", e);
            LOG_INFO.fatal("Error connecting to the data store. Application is stopped.");
            e.printStackTrace();
        }
    }

    static {
        factoryDao.put(AbiturientDao.class, AbiturientDaoImpl.class);
        factoryDao.put(FacultyDao.class, FacultyDaoImpl.class);
        factoryDao.put(SubjectsDao.class, SubjectsDaoImpl.class);
        factoryDao.put(RatingDao.class, RatingDaoImpl.class);
        factoryDao.put(SubjectsListDao.class, SubjectsListDaoImpl.class);
        factoryDao.put(UserDao.class, UserDaoImpl.class);
    }

    @Override
    public DaoImpl getDao(Class<? extends Dao> key) {
        try {
            return factoryDao.get(key).getConstructor(Connection.class).newInstance(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void transactionCommit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error commit transaction.", e);
            LOG_INFO.error("Error commit transaction.");
        }
    }

    @Override
    public void transactionRollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error rollback transaction.", e);
            LOG_INFO.error("Error rollback transaction.");
        }
    }

    @Override
    public void transactionClose() {
        try {
            ConnectionPoolImpl.getConnPool().releaseConnection(connection);
        } catch (Exception e) {
            LOG_DB_ERROR.error("Error release connection.", e);
            LOG_INFO.error("Error release connection.");
        }
    }
}
