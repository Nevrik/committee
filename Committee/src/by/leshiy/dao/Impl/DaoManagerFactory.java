package by.leshiy.dao.Impl;

import by.leshiy.dao.DaoManager;

/**
 * factory for {@link DaoManager}
 * 
 * @author Pavel Liashchou
 * 
 */
public class DaoManagerFactory {

    /**
     * @return instance of {@link DaoManagerImpl}
     */
    public static DaoManager getDaoManager() {
        return new DaoManagerImpl();
    }

}
