package by.leshiy.dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.leshiy.dao.AbiturientDao;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.db.User;

/**
 * Class for work with table Abiturient. Contain methods for CRUD and other
 * operations with data in table Abiturient
 * 
 * @author Pavel Liashchou
 * 
 */
public class AbiturientDaoImpl extends DaoImpl implements AbiturientDao {

    private final String All_COLLUMNS = "id, name, patronomic, surname, diplom_rating, faculty_id, registrationMark, user_id";
    private final String SELECT_SQL = "SELECT " + All_COLLUMNS
            + " FROM Abiturient ORDER BY registrationMark desc, surname";
    private final String SELECT_BY_CHECKED = "SELECT " + All_COLLUMNS
            + " FROM Abiturient WHERE registrationMark = true ORDER BY registrationMark desc";
    private final String SELECT_BY_ID_SQL = "SELECT " + All_COLLUMNS + " FROM Abiturient WHERE id = ?";
    private final String SELECT_BY_FACULTY_SQL = "SELECT " + All_COLLUMNS + " FROM Abiturient WHERE faculty_id = ?";
    private final String SELECT_BY_ROLE_SQL = "SELECT " + All_COLLUMNS + " FROM Abiturient WHERE user_id = ?";
    private final String UPDATE_SQL = "UPDATE Abiturient SET faculty_id = ?, diplom_rating = ?, surname = ?, name = ?, patronomic = ?, registrationMark = ? WHERE id = ?";
    private final String UPDATE_REGISTRATION_MARK = "UPDATE Abiturient SET registrationMark = ? WHERE id = ?";
    private final String DELETE_SQL = "DELETE FROM Abiturient WHERE id = ?";
    private final String ADD_SQL = "INSERT Abiturient (faculty_id, diplom_rating, surname, name, patronomic, registrationMark, user_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private PreparedStatement ps;

    public AbiturientDaoImpl(Connection connection) {
        super(connection);
        super.SELECT_SQL = SELECT_SQL;
        super.SELECT_BY_ID_SQL = SELECT_BY_ID_SQL;
        super.UPDATE_SQL = UPDATE_SQL;
        super.DELETE_SQL = DELETE_SQL;
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.DaoImpl#getByField(java.lang.String, int)
     */
    @Override
    public List<Entity> getByField(String field, int id) {
        switch (field) {
            case "faculty_id" : {
                return getFromDbByField(SELECT_BY_FACULTY_SQL, id);
            }
            case "user_id" : {
                return getFromDbByField(SELECT_BY_ROLE_SQL, id);
            }
            default : {
                return null;
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.DaoImpl#parse(java.sql.ResultSet)
     */
    @Override
    protected List<Entity> parse(ResultSet rs) {
        List<Entity> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Abiturient abitTemp = new Abiturient();
                abitTemp = new Abiturient();
                abitTemp.setId(rs.getInt(1));
                abitTemp.setName(rs.getString(2));
                abitTemp.setPatronomic(rs.getString(3));
                abitTemp.setSurname(rs.getString(4));
                abitTemp.setDiplomRating(rs.getFloat(5));

                Faculty facTemp = new Faculty();
                facTemp.setId((rs.getInt(6)));
                abitTemp.setFaculty(facTemp);

                abitTemp.setRegistrationMark(rs.getBoolean(7));

                User user = new User();
                user.setId(rs.getInt(8));
                abitTemp.setUser(user);

                result.add(abitTemp);
            }
        } catch (NumberFormatException | SQLException e) {
            LOG_DB_ERROR.error("Error during parsing ResultSet. " + this, e);
            LOG_INFO.error("Error during parsing ResultSet. " + this);
        }
        return result;
    }
    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Impl.AbiturientDao#add(by.leshiy.db.Db)
     */
    @Override
    public void add(Entity db) {
        try {
            ps = super.connection.prepareStatement(ADD_SQL);
            ps.setString(4, ((Abiturient) db).getName());
            ps.setString(5, ((Abiturient) db).getPatronomic());
            ps.setString(3, ((Abiturient) db).getSurname());
            ps.setFloat(2, ((Abiturient) db).getDiplomRating());
            ps.setInt(1, ((Abiturient) db).getFaculty().getId());
            ps.setBoolean(6, ((Abiturient) db).isRegistrationMark());
            ps.setInt(7, ((Abiturient) db).getUser().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + ADD_SQL, e);
            LOG_INFO.error("Error execute query " + ADD_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.dao.Dao#update(by.leshiy.db.Entity)
     */
    @Override
    public void update(Entity db) {
        try {
            ps = super.connection.prepareStatement(UPDATE_SQL);
            ps.setString(4, ((Abiturient) db).getName());
            ps.setString(5, ((Abiturient) db).getPatronomic());
            ps.setString(3, ((Abiturient) db).getSurname());
            ps.setFloat(2, ((Abiturient) db).getDiplomRating());
            ps.setInt(1, ((Abiturient) db).getFaculty().getId());
            ps.setBoolean(6, ((Abiturient) db).isRegistrationMark());
            ps.setInt(7, ((Abiturient) db).getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + UPDATE_SQL, e);
            LOG_INFO.error("Error execute query " + UPDATE_SQL);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public List<Entity> getByChecked() {
        return getFromDb(SELECT_BY_CHECKED);
    }

    @Override
    public void setRegistrationMark(boolean newMark, int id) {
        try {
            ps = super.connection.prepareStatement(UPDATE_REGISTRATION_MARK);
            ps.setBoolean(1, newMark);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG_DB_ERROR.error("Error execute query " + UPDATE_REGISTRATION_MARK, e);
            LOG_INFO.error("Error execute query " + UPDATE_REGISTRATION_MARK);
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }
}
