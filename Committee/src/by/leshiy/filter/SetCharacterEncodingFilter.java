package by.leshiy.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class SetCharacterEncodingFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next) throws IOException,
            ServletException {
        // установка UTF-8 кодировки
        request.setCharacterEncoding("UTF-8");
        next.doFilter(request, response);
    }
    public void destroy() {
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {

    }

}
