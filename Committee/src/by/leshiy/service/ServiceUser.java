package by.leshiy.service;

import by.leshiy.db.Entity;

/**
 * @author Pavel Liashchou
 * 
 */
public interface ServiceUser extends Service {

    /**
     * Find records from table 'User' where login = 'login'
     * 
     * @param login
     *            parameter for searching query
     * @return record that has field login equals parameter 'login'
     */
    Entity findByLogin(String login);
}