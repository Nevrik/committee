package by.leshiy.service;

import java.sql.SQLException;
import java.util.List;

import by.leshiy.dao.DaoManager;
import by.leshiy.db.Entity;

/**
 * @author Pavel Liashchou
 * 
 */
public interface Service {

    /**
     * @return instance of transaction manager DaoManager
     */
    DaoManager getManager();

    /**
     * Find records from database table corresponding Entity by 'id'
     * 
     * @param id
     *            id of records that want to find
     * @return list of records from database table corresponding Entity by 'id'
     */
    List<Entity> findById(int id);

    /**
     * Find all records from database table corresponding Entity
     * 
     * @return list of all records from database table corresponding Entity
     */
    List<Entity> findAll();

    /**
     * Delete record 'db' from data store corresponding Entity
     * 
     * @param db
     *            record for deleting
     */
    void delete(Entity db);

    /**
     * Insert/update record 'db' from data store corresponding Entity.<br>
     * Insert new record if db.getId() == null, else - update.
     * 
     * @param db
     *            record for adding/updating
     * @throws SQLException
     */
    void add(Entity db);
}
