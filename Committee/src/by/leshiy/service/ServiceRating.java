package by.leshiy.service;

import java.util.List;

import by.leshiy.db.Entity;

/**
 * @author Pavel Liashchou
 * 
 */
public interface ServiceRating extends Service {

    /**
     * Find records from table 'Rating' where abiturient_id = 'id'
     * 
     * @param id
     *            parameter for searching query
     * @return list of records that has field abiturient_id equals parameter
     *         'id'
     */
    List<Entity> findByAbiturient(int id);

    /**
     * Find records from table 'Rating' where subjects_id = 'id'
     * 
     * @param id
     *            parameter for searching query
     * @return list of records that has field subjects_id equals parameter 'id'
     */
    List<Entity> findBySubject(int id);

}