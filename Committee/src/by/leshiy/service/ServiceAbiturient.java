package by.leshiy.service;

import java.util.List;

import by.leshiy.db.Entity;

/**
 * Service methods for the table Abiturient
 * 
 * @author Pavel Liashchou
 * 
 */
public interface ServiceAbiturient extends Service {

    /**
     * Find records from table 'Abiturient' where faculty_id = 'id'
     * 
     * @param id
     *            parameter for searching query
     * @return list of records that has field faculty_id equals parameter 'id'
     */
    List<Entity> findByFaculty(int id);

    /**
     * @return list of records that registrationMark = true
     */
    List<Entity> findChecked();

    /**
     * @param newMark
     *            true or false - new registrationMark
     * @param id
     *            identity of the record that need to change
     */
    void setRegistrationMark(boolean newMark, int id);

    /**
     * Find records from table 'Abiturient' where user_id = 'id'
     * 
     * @param id
     *            parameter for searching query
     * @return list of records that has field user_id equals parameter 'id'
     */
    List<Entity> findByUser(int id);
}