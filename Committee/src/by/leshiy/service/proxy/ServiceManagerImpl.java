package by.leshiy.service.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import by.leshiy.dao.DaoManager;
import by.leshiy.dao.Impl.DaoManagerFactory;
import by.leshiy.service.Service;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.ServiceRating;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.ServiceUser;
import by.leshiy.service.Impl.ServiceAbiturientImpl;
import by.leshiy.service.Impl.ServiceFacultyImpl;
import by.leshiy.service.Impl.ServiceRatingImpl;
import by.leshiy.service.Impl.ServiceSubjectsImpl;
import by.leshiy.service.Impl.ServiceUserImpl;

/**
 * @author Pavel Liashchou
 * 
 */
public class ServiceManagerImpl implements ServiceManager {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private static Map<Class<? extends Service>, Class<? extends Service>> services = new ConcurrentHashMap<>();

    static {
        services.put(ServiceAbiturient.class, ServiceAbiturientImpl.class);
        services.put(ServiceFaculty.class, ServiceFacultyImpl.class);
        services.put(ServiceSubjects.class, ServiceSubjectsImpl.class);
        services.put(ServiceRating.class, ServiceRatingImpl.class);
        services.put(ServiceUser.class, ServiceUserImpl.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <Type extends Service> Type getService(Class<Type> key) throws Exception {
        Class<? extends Service> value = services.get(key);
        if (value != null) {
            ClassLoader classLoader = value.getClassLoader();
            Class<?>[] interfaces = {key};
            Service service = null;
            service = value.getConstructor(DaoManager.class).newInstance(DaoManagerFactory.getDaoManager());
            InvocationHandler handler = new InvocationHandlerImpl(service);
            return (Type) Proxy.newProxyInstance(classLoader, interfaces, handler);
        }
        return null;
    }
}
