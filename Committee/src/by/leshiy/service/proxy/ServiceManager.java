package by.leshiy.service.proxy;

import java.io.Serializable;

import by.leshiy.service.Service;

/**
 * @author Pavel Liashchou
 * 
 */
public interface ServiceManager extends Serializable {

    /**
     * @param key
     *            interface of getting service
     * @return service class
     * @throws Exception
     */
    public <Type extends Service> Type getService(Class<Type> key) throws Exception;
}
