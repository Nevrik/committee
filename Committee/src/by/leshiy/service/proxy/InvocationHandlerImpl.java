package by.leshiy.service.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import by.leshiy.service.Service;

/**
 * @author Pavel Liashchou
 * 
 */
public class InvocationHandlerImpl implements InvocationHandler {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private Service service;

    public InvocationHandlerImpl(Service service) {
        this.service = service;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] arguments) throws Exception {
        try {
            Object result = method.invoke(service, arguments);
            service.getManager().transactionCommit();
            return result;
        } catch (Exception e) {
            try {
                service.getManager().transactionRollback();
            } catch (Exception e1) {
                LOG_DB_ERROR.error("Error execute query. Transaction rallback.", e);
                LOG_INFO.error("Error execute query ");
            }
            throw e;
        } finally {
            service.getManager().transactionClose();
        }
    }
}