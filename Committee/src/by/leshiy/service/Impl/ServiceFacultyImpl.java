package by.leshiy.service.Impl;

import java.util.ArrayList;
import java.util.List;

import by.leshiy.dao.Dao;
import by.leshiy.dao.DaoManager;
import by.leshiy.dao.FacultyDao;
import by.leshiy.dao.SubjectsDao;
import by.leshiy.dao.SubjectsListDao;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.db.Subjects;
import by.leshiy.db.SubjectsList;
import by.leshiy.service.ServiceFaculty;

/**
 * @author Pavel Liashchou
 * 
 */
public class ServiceFacultyImpl extends ServiceImpl implements ServiceFaculty {

    public ServiceFacultyImpl(DaoManager manager) {
        super(manager);
    }

    private Dao daoFac = getManager().getDao((Class<? extends Dao>) FacultyDao.class);
    private Dao daoSub = getManager().getDao((Class<? extends Dao>) SubjectsDao.class);
    private Dao daoSubList = getManager().getDao((Class<? extends Dao>) SubjectsListDao.class);

    /**
     * Filling each Entity from listFaculty data from tables Subject,
     * SubjectList
     * 
     * @param listFaculty
     * @return completely filled list of entity
     */
    private List<Entity> find(List<Entity> listFaculty) {
        List<Entity> subjId = new ArrayList<>();
        for (Entity f : listFaculty) {
            List<Subjects> listSubjects = new ArrayList<Subjects>();
            int idF = f.getId();
            subjId = daoSubList.getByField("faculty_id", idF);
            if (subjId.size() > 0) {
                for (Entity s : subjId) {
                    List<Entity> subjects = daoSub.getById((((SubjectsList) s).getSubjects().getId()));
                    listSubjects.add((Subjects) subjects.get(0));
                }
                ((Faculty) f).setSubjects(listSubjects);
            }
        }
        return listFaculty;
    }
    @Override
    public List<Entity> findById(int id) {
        return find(daoFac.getById(id));
    }

    @Override
    public List<Entity> findAll() {
        return find(daoFac.getAll());
    }

    @Override
    public void delete(Entity db) {
        daoFac.delete(db);
    }

    @Override
    public void add(Entity faculty) {
        if (faculty.getId() != 0) {
            daoFac.update(faculty);
            List<Entity> subListId = daoSubList.getByField("faculty_id", faculty.getId());
            for (Entity sub : subListId) {
                daoSubList.delete(sub);
            }
            for (Subjects s : ((Faculty) faculty).getSubjects()) {
                SubjectsList l = new SubjectsList();
                l.setFaculty((Faculty) faculty);
                l.setSubjects(s);
                daoSubList.add(l);
            }
        } else {
            daoFac.add(faculty);
        }
    }
}
