package by.leshiy.service.Impl;

import java.util.ArrayList;
import java.util.List;

import by.leshiy.dao.AbiturientDao;
import by.leshiy.dao.Dao;
import by.leshiy.dao.DaoManager;
import by.leshiy.dao.FacultyDao;
import by.leshiy.dao.RatingDao;
import by.leshiy.dao.SubjectsDao;
import by.leshiy.dao.UserDao;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.db.Rating;
import by.leshiy.db.Subjects;
import by.leshiy.db.User;
import by.leshiy.service.ServiceAbiturient;

/**
 * @author Pavel Liashchou
 * 
 */
public class ServiceAbiturientImpl extends ServiceImpl implements ServiceAbiturient {

    public ServiceAbiturientImpl(DaoManager manager) {
        super(manager);
    }

    private Dao daoAb = getManager().getDao(AbiturientDao.class);
    private Dao daoUser = getManager().getDao(UserDao.class);
    private Dao daoFac = getManager().getDao(FacultyDao.class);
    private Dao daoRating = getManager().getDao(RatingDao.class);
    private Dao daoSub = getManager().getDao((Class<? extends Dao>) SubjectsDao.class);

    @Override
    public List<Entity> findById(int id) {
        return find(daoAb.getById(id));
    }

    @Override
    public List<Entity> findByFaculty(int id) {
        return find(daoAb.getByField("faculty_id", id));
    }

    @Override
    public List<Entity> findByUser(int id) {
        return find(daoAb.getByField("user_id", id));
    }

    @Override
    public List<Entity> findAll() {
        return find(daoAb.getAll());
    }

    /**
     * Filling each Entity from listAbit data from tables Faculty, User, Rating
     * 
     * @param listAbit
     * @return completely filled list of entity
     */
    private List<Entity> find(List<Entity> listAbit) {
        List<Entity> raitingTemp = new ArrayList<>();
        if (listAbit != null) {
            for (Entity d : listAbit) {
                int id = ((Abiturient) d).getFaculty().getId();
                ((Abiturient) d).setFaculty((Faculty) daoFac.getById(id).get(0));
                ((Abiturient) d).setUser((User) daoUser.getById(((Abiturient) d).getUser().getId()).get(0));
                raitingTemp = daoRating.getByField("abiturient_id", d.getId());
                for (Entity r : raitingTemp) {
                    int idSub = ((Rating) r).getSubjects().getId();
                    int idAbit = ((Rating) r).getAbiturient().getId();
                    ((Rating) r).setAbiturient((Abiturient) daoAb.getById(idAbit).get(0));
                    ((Rating) r).setSubjects((Subjects) daoSub.getById(idSub).get(0));
                }
                ((Abiturient) d).setRating(raitingTemp);
            }
        }

        return listAbit;
    }
    @Override
    public void delete(Entity db) {
        daoAb.delete(db);
    }

    @Override
    public void add(Entity abiturient) {
        if (abiturient.getId() != 0) {
            daoAb.update(abiturient);
            List<Entity> ratingForDel = daoRating.getByField("abiturient_id", abiturient.getId());
            for (Entity rat : ratingForDel) {
                daoRating.delete(rat);
            }
            for (Entity r : ((Abiturient) abiturient).getRating()) {
                daoRating.add(r);
            }
        } else {
            daoAb.add(abiturient);
            abiturient.setId(daoAb.getByField("user_id", ((Abiturient) abiturient).getUser().getId()).get(0).getId());
            for (Entity r : ((Abiturient) abiturient).getRating()) {
                daoRating.add(r);
            }
        }
    }
    @Override
    public List<Entity> findChecked() {
        return find(((AbiturientDao) daoAb).getByChecked());
    }

    @Override
    public void setRegistrationMark(boolean newMark, int id) {
        ((AbiturientDao) daoAb).setRegistrationMark(newMark, id);
    }

}
