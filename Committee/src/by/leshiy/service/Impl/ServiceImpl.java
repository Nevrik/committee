package by.leshiy.service.Impl;

import by.leshiy.dao.DaoManager;
import by.leshiy.service.Service;

/**
 * @author Pavel Liashchou
 * 
 */
public abstract class ServiceImpl implements Service {

    private DaoManager manager = null;

    public ServiceImpl(DaoManager manager) {
        this.manager = manager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see by.leshiy.service.Service#getManager()
     */
    @Override
    public DaoManager getManager() {
        return manager;
    }

}
