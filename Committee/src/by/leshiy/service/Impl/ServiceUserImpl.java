package by.leshiy.service.Impl;

import java.util.List;

import by.leshiy.dao.Dao;
import by.leshiy.dao.DaoManager;
import by.leshiy.dao.UserDao;
import by.leshiy.db.Entity;
import by.leshiy.service.ServiceUser;

/**
 * @author Pavel Liashchou
 * 
 */
public class ServiceUserImpl extends ServiceImpl implements ServiceUser {

    private Dao daoUser = getManager().getDao(UserDao.class);

    public ServiceUserImpl(DaoManager manager) {
        super(manager);
    }

    @Override
    public List<Entity> findById(int id) {
        return daoUser.getById(id);
    }

    @Override
    public List<Entity> findAll() {
        return daoUser.getAll();
    }

    @Override
    public void delete(Entity db) {
        daoUser.delete(db);
    }

    @Override
    public void add(Entity db) {
        daoUser.add(db);
    }

    @Override
    public Entity findByLogin(String login) {
        return (Entity) ((UserDao) daoUser).getByLogin(login);
    }
}