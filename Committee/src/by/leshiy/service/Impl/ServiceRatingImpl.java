package by.leshiy.service.Impl;

import java.util.List;

import by.leshiy.dao.AbiturientDao;
import by.leshiy.dao.Dao;
import by.leshiy.dao.DaoManager;
import by.leshiy.dao.RatingDao;
import by.leshiy.dao.SubjectsDao;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Rating;
import by.leshiy.db.Subjects;
import by.leshiy.service.ServiceRating;

/**
 * @author Pavel Liashchou
 * 
 */
public class ServiceRatingImpl extends ServiceImpl implements ServiceRating {

    public ServiceRatingImpl(DaoManager manager) {
        super(manager);
    }

    private Dao daoRating = getManager().getDao(RatingDao.class);
    private Dao daoAbiturient = getManager().getDao(AbiturientDao.class);
    private Dao daoSubject = getManager().getDao(SubjectsDao.class);

    /**
     * Filling each Entity from listRating data from tables Subject, Abiturient
     * 
     * @param listRating
     * @return completely filled list of entity
     */
    private List<Entity> find(List<Entity> listRating) {
        for (Entity rat : listRating) {
            ((Rating) rat).setSubjects((Subjects) daoSubject.getById(
                    ((Rating) rat).getSubjects().getId()).get(0));
            ((Rating) rat).setAbiturient((Abiturient) daoAbiturient.getById(
                    ((Rating) rat).getAbiturient().getId()).get(0));
        }
        return listRating;
    }

    @Override
    public List<Entity> findByAbiturient(int id) {
        return find(daoRating.getByField("abiturient_id", id));
    }

    @Override
    public List<Entity> findBySubject(int id) {
        return find(daoRating.getByField("subject_id", id));
    }

    @Override
    public List<Entity> findById(int id) {
        return find(daoRating.getById(id));
    }

    @Override
    public List<Entity> findAll() {
        return find(daoRating.getAll());
    }

    @Override
    public void delete(Entity db) {

    }

    @Override
    public void add(Entity subject) {

    }
}
