package by.leshiy.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.leshiy.controller.command.Action;
import by.leshiy.controller.command.ActionFactory;
import by.leshiy.db.Role;
import by.leshiy.dbHelp.ConnectionPoolImpl;

/**
 * Dispatcher
 * 
 * first and alone Servlet in application<br>
 * 
 * @author Pavel Liashchou
 * 
 */
public class Controller extends HttpServlet implements Servlet {

    ActionFactory beanFactory;
    Action bean = null;
    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");

    /**
     * redirect request/response in method processingRequest
     * 
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
     *      , javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processingRequest(req, resp);
    }

    /**
     * redirect request/response in method processingRequest
     * 
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processingRequest(req, resp);
    }

    /**
     * - if COMMAND = logoutAction, go to the action <br>
     * LogoutAction(redirection to the main page, session will be cleared) <br>
     * <br>
     * - if not logoutAction, call method forwardRequest
     * 
     * @throws IOException
     */
    private void processingRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            req.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        if (req.getParameter("COMMAND") != null && req.getParameter("COMMAND").equals("logoutAction")) {
            bean = beanFactory.getAction(req.getParameter("COMMAND"));
            try {
                bean.execute(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                forwardRequest(req, resp);
            } catch (IOException e) {
                resp.sendRedirect("errorDb.html");
            } catch (ServletException e) {
                resp.sendRedirect("errorDb.html");
            }
        }
    }

    /**
     * - Call Command instance from HashMap in {@link ActionFactory} by key in
     * request.getParameter("COMMAND")<br>
     * - Put instance of Command in the session if it is not there <br>
     * - Forward to a page that return from Command.execute method
     * 
     * @param req
     * @param resp
     * @throws IOException
     * @throws ServletException
     */
    private void forwardRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("UTF-8");
        if (getServletConfig().getServletContext().getAttribute("error") == null) {
            String pages;
            HttpSession session = req.getSession();

            try {
                if (req.getParameter("COMMAND") != null) {
                    // System.out.println(req.getParameter("COMMAND"));
                    bean = beanFactory.getBean(req.getParameter("COMMAND"), (session.getAttribute("role") == null)
                            ? null
                            : ((Role) session.getAttribute("role")).toString());
                    pages = bean.execute(req, resp);
                } else {
                    pages = beanFactory.getAction("firstPage").execute(req, resp);
                }
            } catch (Exception e) {
                pages = "/index.jsp";
                // System.out.println("Переход на стартовую страницу.");
                e.printStackTrace();
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(pages);
            rd.forward(req, resp);
        } else {
            resp.sendRedirect("errorDb.html");
        }
    }

    /**
     * initialize ConnectionPool and BeanFactory
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            ConnectionPoolImpl.getConnPool();
            beanFactory = ActionFactory.getInstance();
            LOG_INFO.info("Running OK.");
            System.out.println("Сервлет загружен успешно.");
        } catch (Exception e) {
            config.getServletContext().setAttribute("error", "db");
            LOG_DB_ERROR.fatal("Error connecting to the data store. Application is stopped.", e);
            LOG_INFO.fatal("Error connecting to the data store. Application is stopped.");
            destroy();
        }
        super.init(config);
    }

    /**
     * close ConnectionPool, destroy servlet Controller
     */
    @Override
    public void destroy() {
        System.out.println("Servlet was destroyed!");
        try {
            ConnectionPoolImpl.getConnPool().closeAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.destroy();
    }
}