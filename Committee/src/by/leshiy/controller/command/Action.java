package by.leshiy.controller.command;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Action extends Serializable {

    String execute(HttpServletRequest request, HttpServletResponse response) throws Exception;
}