package by.leshiy.controller.command;

import by.leshiy.controller.command.actions.FirstPageAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientEditAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientShowAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientsListAction;
import by.leshiy.controller.command.actions.abiturients.NewAbiturientAction;
import by.leshiy.controller.command.actions.actions.AbiturientSaveAction;
import by.leshiy.controller.command.actions.actions.ChangeRegistrationMarkAction;
import by.leshiy.controller.command.actions.actions.FacultySaveAction;
import by.leshiy.controller.command.actions.actions.LoginAction;
import by.leshiy.controller.command.actions.actions.LogoutAction;
import by.leshiy.controller.command.actions.actions.NewUserSaveAction;
import by.leshiy.controller.command.actions.actions.RecoveryPasswordAction;
import by.leshiy.controller.command.actions.actions.SubjectSaveAction;
import by.leshiy.controller.command.actions.faculty.FacultyEditAction;
import by.leshiy.controller.command.actions.faculty.FacultyListAction;
import by.leshiy.controller.command.actions.statement.StatementAction;
import by.leshiy.controller.command.actions.subjects.SubjectEditAction;
import by.leshiy.controller.command.actions.subjects.SubjectsListAction;
import by.leshiy.controller.command.actions.user.NewUserAction;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * Contains Multimap with actions and roles that correspond to the action.<br>
 * Selection takes place by command class extends Bean.<br>
 * 
 * @author Pavel Liashchou
 * 
 */
public class RoleManager {

    private static RoleManager instance;
    private Multimap<Class<? extends Action>, String> multimap = ArrayListMultimap.create();

    private RoleManager() {
        grant(LogoutAction.class, "ADMINISTRATOR");
        grant(LogoutAction.class, "ABITURIENT");

        grant(AbiturientEditAction.class, "ADMINISTRATOR");
        grant(AbiturientEditAction.class, "ABITURIENT");

        grant(AbiturientSaveAction.class, "ADMINISTRATOR");
        grant(AbiturientSaveAction.class, "ABITURIENT");

        grant(AbiturientsListAction.class, "ADMINISTRATOR");
        grant(AbiturientsListAction.class, "ABITURIENT");

        grant(NewAbiturientAction.class, "ADMINISTRATOR");
        grant(NewAbiturientAction.class, "ABITURIENT");

        grant(RecoveryPasswordAction.class, null);

        grant(LoginAction.class, "ADMINISTRATOR");
        grant(LoginAction.class, "ABITURIENT");
        grant(LoginAction.class, null);

        grant(NewUserAction.class, null);

        grant(NewUserSaveAction.class, null);

        grant(FirstPageAction.class, "ADMINISTRATOR");
        grant(FirstPageAction.class, "ABITURIENT");
        grant(FirstPageAction.class, null);

        grant(FacultySaveAction.class, "ADMINISTRATOR");
        grant(FacultyEditAction.class, "ADMINISTRATOR");
        grant(FacultyListAction.class, "ADMINISTRATOR");
        grant(SubjectsListAction.class, "ADMINISTRATOR");
        grant(SubjectEditAction.class, "ADMINISTRATOR");
        grant(SubjectSaveAction.class, "ADMINISTRATOR");
        grant(StatementAction.class, "ADMINISTRATOR");
        grant(ChangeRegistrationMarkAction.class, "ADMINISTRATOR");
        grant(AbiturientShowAction.class, "ABITURIENT");
        grant(AbiturientShowAction.class, "ADMINISTRATOR");

    }

    /**
     * @return instance of the {@link RoleManager}
     */
    // TODO: rebuild singleton
    public static RoleManager getInstance() {
        if (instance == null)
            return instance = new RoleManager();
        else
            return instance;
    }

    public void grant(Class<? extends Action> employee, String role) {
        multimap.put(employee, role);
    }

    public Multimap<Class<? extends Action>, String> getMultimap() {
        return multimap;
    }
}
