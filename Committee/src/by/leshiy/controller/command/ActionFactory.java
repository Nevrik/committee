package by.leshiy.controller.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.leshiy.controller.command.actions.FirstPageAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientEditAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientShowAction;
import by.leshiy.controller.command.actions.abiturients.AbiturientsListAction;
import by.leshiy.controller.command.actions.abiturients.NewAbiturientAction;
import by.leshiy.controller.command.actions.actions.AbiturientSaveAction;
import by.leshiy.controller.command.actions.actions.ChangeRegistrationMarkAction;
import by.leshiy.controller.command.actions.actions.FacultySaveAction;
import by.leshiy.controller.command.actions.actions.LoginAction;
import by.leshiy.controller.command.actions.actions.LogoutAction;
import by.leshiy.controller.command.actions.actions.NewUserSaveAction;
import by.leshiy.controller.command.actions.actions.RecoveryPasswordAction;
import by.leshiy.controller.command.actions.actions.SubjectSaveAction;
import by.leshiy.controller.command.actions.faculty.FacultyEditAction;
import by.leshiy.controller.command.actions.faculty.FacultyListAction;
import by.leshiy.controller.command.actions.statement.StatementAction;
import by.leshiy.controller.command.actions.subjects.SubjectEditAction;
import by.leshiy.controller.command.actions.subjects.SubjectsListAction;
import by.leshiy.controller.command.actions.user.NewUserAction;

/**
 * Contains HashMap with all actions.<br>
 * Selection of the action takes place by String key.<br>
 * 
 * @author Pavel Liashchou
 * 
 */
public class ActionFactory {

    private static ActionFactory instance;
    private Map<String, Action> action = new HashMap<>();
    private RoleManager roleManager = RoleManager.getInstance();

    private ActionFactory() {
        action.put("firstPage", new FirstPageAction());
        action.put("editFaculty", new FacultyEditAction());
        action.put("saveFaculty", new FacultySaveAction());
        action.put("showFacultyList", new FacultyListAction());
        action.put("showSubjectsList", new SubjectsListAction());
        action.put("editSubject", new SubjectEditAction());
        action.put("saveSubject", new SubjectSaveAction());
        action.put("editAbiturient", new AbiturientEditAction());
        action.put("saveAbiturient", new AbiturientSaveAction());
        action.put("showAbitList", new AbiturientsListAction());
        action.put("recoveryPassword", new RecoveryPasswordAction());
        action.put("loginAction", new LoginAction());
        action.put("logoutAction", new LogoutAction());
        action.put("newAbiturient", new NewAbiturientAction());
        action.put("newUserBean", new NewUserAction());
        action.put("newUserSave", new NewUserSaveAction());
        action.put("statement", new StatementAction());
        action.put("changeRegistrationMark", new ChangeRegistrationMarkAction());
        action.put("showAbiturient", new AbiturientShowAction());
    }

    /**
     * @return instance of the BeanFactory
     */
    // TODO: rebuild singleton
    public static ActionFactory getInstance() {
        if (instance == null)
            return instance = new ActionFactory();
        else
            return instance;
    }

    /**
     * 
     * @param s
     *            key
     * @return action by key
     */
    public Action getAction(String s) {
        return action.get(s);
    }

    public Action getBean(String s, String role) {
        Action tempAction = null;
        List<String> roles = (List<String>) roleManager.getMultimap().get(action.get(s).getClass());
        for (String r : roles) {
            if (r == role) {
                tempAction = action.get(s);
            }
        }
        return tempAction;
    }
}
