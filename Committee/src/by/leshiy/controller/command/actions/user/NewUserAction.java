package by.leshiy.controller.command.actions.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;

public class NewUserAction implements Action {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return "/WEB-INF/jsp/user/newUser.jsp";
    }
}