package by.leshiy.controller.command.actions.abiturients;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Rating;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * Action adds to the request attributes to display the page
 * 'abiturients/edit.jsp'
 * 
 * @author Pavel Liashchou
 * 
 */
public class AbiturientEditAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();
    private int idAbiturient, faculty;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        faculty = Integer.parseInt(request.getParameter("faculty"));
        try {
            idAbiturient = Integer.parseInt(request.getParameter("idAbiturient"));
            Abiturient abiturient = (Abiturient) manager.getService(ServiceAbiturient.class).findById(idAbiturient)
                    .get(0);
            int result = 0;
            result += abiturient.getDiplomRating() * 10;
            for (int i = 0; i < abiturient.getRating().size(); i++) {
                result += ((Rating) abiturient.getRating().get(i)).getMark();
                request.setAttribute("abiturient", abiturient);
                request.setAttribute("result", result);
            }
        } catch (NumberFormatException e) {
        }
        request.setAttribute("faculty", manager.getService(ServiceFaculty.class).findById(faculty).get(0));
        request.setAttribute("facultyList", manager.getService(ServiceFaculty.class).findAll());
        return "/WEB-INF/jsp/abiturients/edit.jsp";
    }
}
