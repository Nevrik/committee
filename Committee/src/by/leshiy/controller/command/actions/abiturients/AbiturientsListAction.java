package by.leshiy.controller.command.actions.abiturients;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * Action adds to the request attributes to display the page
 * 'abiturients/abiturients.jsp'
 * 
 * @author Pavel Liashchou
 * 
 */
public class AbiturientsListAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();
    private int id = 1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (request.getParameter("facultySelect") != null) {
            id = Integer.parseInt((String) request.getParameter("facultySelect"));
        }
        request.setAttribute("facultyList", manager.getService(ServiceFaculty.class).findAll());
        request.setAttribute("abiturientList", manager.getService(ServiceAbiturient.class)
                .findByFaculty(id));
        return "/WEB-INF/jsp/abiturients/abiturients.jsp";
    }
}