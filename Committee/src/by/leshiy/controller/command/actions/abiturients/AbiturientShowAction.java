package by.leshiy.controller.command.actions.abiturients;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Rating;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * Action adds to the request attributes to display the main page for role
 * ABITURIENT
 * 
 * @author Pavel Liashchou
 * 
 */
public class AbiturientShowAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String page = "/main?COMMAND=firstPage";
        int result = 0;
        if (!session.getAttribute("role").equals("ADMINISTRATOR")) {
            Abiturient abiturient = (Abiturient) manager.getService(ServiceAbiturient.class)
                    .findByUser((int) session.getAttribute("userId")).get(0);
            result += abiturient.getDiplomRating() * 10;
            for (int i = 0; i < abiturient.getRating().size(); i++) {
                result += ((Rating) abiturient.getRating().get(i)).getMark();
            }
            request.setAttribute("abiturient", abiturient);
            request.setAttribute("result", result);
        }
        return page;
    }
}