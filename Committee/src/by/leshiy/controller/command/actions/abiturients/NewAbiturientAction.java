package by.leshiy.controller.command.actions.abiturients;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * Action adds to the request attributes to display the page
 * 'abiturients/newAbiturient.jsp'
 * 
 * @author Pavel Liashchou
 * 
 */
public class NewAbiturientAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();
    private int idFaculty;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getParameter("idFaculty") != null) {
            idFaculty = Integer.parseInt(request.getParameter("idFaculty"));
            request.setAttribute("selectedFaculty", manager.getService(ServiceFaculty.class).findById(idFaculty).get(0));
        }
        request.setAttribute("faculty", manager.getService(ServiceFaculty.class).findAll());

        return "/WEB-INF/jsp/abiturients/newAbiturient.jsp";
    }
}