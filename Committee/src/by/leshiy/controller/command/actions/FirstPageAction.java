package by.leshiy.controller.command.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.leshiy.controller.command.Action;
import by.leshiy.controller.command.actions.statement.FacultyCheckRating;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Rating;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class FirstPageAction implements Action {

    private List<Entity> facultyList = new ArrayList<>();
    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            int result = 0;
            try {
                Abiturient abiturient = (Abiturient) manager.getService(ServiceAbiturient.class)
                        .findByUser((int) session.getAttribute("userId")).get(0);
                result += (int) (abiturient.getDiplomRating() * 10);
                for (int i = 0; i < abiturient.getRating().size(); i++) {
                    result += ((Rating) abiturient.getRating().get(i)).getMark();
                }
                request.setAttribute("abiturient", abiturient);
                request.setAttribute("result", result);
            } catch (IndexOutOfBoundsException e) {
            }
        }
        facultyList = manager.getService(ServiceFaculty.class).findAll();
        for (Entity fac : facultyList) {
            FacultyCheckRating.setFacultyCheckRating(fac);
        }
        request.setAttribute("facultyList", facultyList);
        return "/WEB-INF/main.jsp";
    }
}