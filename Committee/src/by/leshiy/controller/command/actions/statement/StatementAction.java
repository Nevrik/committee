package by.leshiy.controller.command.actions.statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class StatementAction implements Action {

    private List<Entity> faculty = new ArrayList<>();
    private List<Entity> abiturient = new ArrayList<>();
    private ServiceManager manager = new ServiceManagerImpl();
    private StatementCompareByMarkDesc<Entity> comparatorDesc = new StatementCompareByMarkDesc<Entity>();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        abiturient = manager.getService(ServiceAbiturient.class).findChecked();
        Collections.sort(abiturient, comparatorDesc);
        for (Entity ab : abiturient) {
            ((Abiturient) ab).setDiplomRating(((Abiturient) ab).getDiplomRating() * 10);
        }
        faculty = manager.getService(ServiceFaculty.class).findAll();
        try {
            for (Entity fac : faculty) {
                FacultyCheckRating.setFacultyCheckRating(fac);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("facultyList", faculty);
        request.setAttribute("abiturientList", abiturient);
        return "/WEB-INF/jsp/statement.jsp";
    }
}