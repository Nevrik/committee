package by.leshiy.controller.command.actions.statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.db.Rating;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public abstract class FacultyCheckRating {
    
    private static ServiceManager manager = new ServiceManagerImpl();
    private static StatementCompareByMarkDesc<Entity> comparatorDesc = new StatementCompareByMarkDesc<Entity>();
    
    public static void setFacultyCheckRating(Entity fac) throws Exception {
        List<Entity> listAbit = new ArrayList<Entity>();
        List<Entity> tempListAbit = new ArrayList<Entity>();
        tempListAbit = manager.getService(ServiceAbiturient.class).findByFaculty(fac.getId());
        for (Entity ab : tempListAbit) {
            if (((Abiturient) ab).isRegistrationMark()) {
                listAbit.add(ab);
            }
        }
        // sort by summary rating
        Collections.sort(listAbit, comparatorDesc);
        if (((Faculty) fac).getRecruitPlan() == 0) {
            ((Faculty) fac).setCheckRating(0);
        } else {
            try {
                ((Faculty) fac).setCheckRating(allMark(listAbit.get(((Faculty) fac).getRecruitPlan() - 1)));
            } catch (IndexOutOfBoundsException e) {
                try {
                    ((Faculty) fac).setCheckRating(allMark(listAbit.get(listAbit.size() - 1)));
                } catch (ArrayIndexOutOfBoundsException e2) {
                    ((Faculty) fac).setCheckRating(0);
                }
            }
        }
    }
    
    private static int allMark(Entity ab) {
        int markSum = (int) (((Abiturient) ab).getDiplomRating() * 10);
        for (Entity rating : ((Abiturient) ab).getRating()) {
            markSum += ((Rating) rating).getMark();
        }
        return markSum;
    }
}
