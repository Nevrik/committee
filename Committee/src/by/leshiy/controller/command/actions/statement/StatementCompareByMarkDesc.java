package by.leshiy.controller.command.actions.statement;

import java.util.Comparator;

import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Rating;

public class StatementCompareByMarkDesc<T extends Entity> implements Comparator<T> {

    @Override
    public int compare(T arg0, T arg1) {
        int mark1 = ((int) (((Abiturient) arg0).getDiplomRating() * 10)) + markSum(arg0);
        int mark2 = ((int) (((Abiturient) arg1).getDiplomRating() * 10)) + markSum(arg1);
        return (mark2 - mark1);
    }

    private int markSum(T ab) {
        int markSum = 0;
        for (Entity rating : ((Abiturient) ab).getRating()) {
            markSum += ((Rating) rating).getMark();
        }
        return markSum;
    }
}
