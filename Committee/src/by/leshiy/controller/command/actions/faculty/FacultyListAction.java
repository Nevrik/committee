package by.leshiy.controller.command.actions.faculty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.controller.command.actions.statement.FacultyCheckRating;
import by.leshiy.db.Entity;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class FacultyListAction implements Action {

    private List<Entity> faculty = new ArrayList<>();
    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        faculty = manager.getService(ServiceFaculty.class).findAll();
        for (Entity fac : faculty) {
            FacultyCheckRating.setFacultyCheckRating(fac);
        }
        request.setAttribute("facultyList", faculty);
        return "/WEB-INF/jsp/faculty/faculty.jsp";
    }
}
