package by.leshiy.controller.command.actions.faculty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * @author Pavel Liashchou
 * 
 */
public class FacultyEditAction implements Action {

    private Faculty faculty = new Faculty();
    private List<Entity> subject = new ArrayList<>();
    private ServiceManager manager = new ServiceManagerImpl();
    private int id;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        id = Integer.parseInt(request.getParameter("faculty"));
        faculty = (Faculty) manager.getService(ServiceFaculty.class).findById(id).get(0);
        subject = manager.getService(ServiceSubjects.class).findAll();
        request.setAttribute("faculty", faculty);
        request.setAttribute("subjectList", subject);
        return "/WEB-INF/jsp/faculty/edit.jsp";
    }
}
