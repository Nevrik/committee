package by.leshiy.controller.command.actions.subjects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * @author Pavel Liashchou
 * 
 */
public class SubjectsListAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        request.setAttribute("subject", manager.getService(ServiceSubjects.class).findAll());
        return "/WEB-INF/jsp/subjects/subjects.jsp";
    }
}