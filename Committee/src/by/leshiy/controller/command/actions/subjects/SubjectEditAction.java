package by.leshiy.controller.command.actions.subjects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * @author Pavel Liashchou
 * 
 */
public class SubjectEditAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();
    private int id;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        id = Integer.parseInt(request.getParameter("idSubject"));
        request.setAttribute("subject",
                manager.getService(ServiceSubjects.class).findById(id).get(0));
        return "/WEB-INF/jsp/subjects/edit.jsp";
    }
}