package by.leshiy.controller.command.actions.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Subjects;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class SubjectSaveAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Subjects subject = new Subjects();
        subject.setName(request.getParameter("editingSubject"));
        subject.setId(Integer.parseInt(request.getParameter("idSubject")));
        try {
            manager.getService(ServiceSubjects.class).add(subject);
            request.setAttribute("save", "Сохранено!");
        } catch (Exception e) {
            request.setAttribute("save", "ERROR!");
            e.printStackTrace();
        }
        return "/main?COMMAND=editSubject";
    }
}