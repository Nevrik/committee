package by.leshiy.controller.command.actions.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Abiturient;
import by.leshiy.db.Entity;
import by.leshiy.db.Faculty;
import by.leshiy.db.Rating;
import by.leshiy.db.Subjects;
import by.leshiy.db.User;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class AbiturientSaveAction implements Action {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private ServiceManager manager = new ServiceManagerImpl();
    private int idFaculty, id;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        Abiturient abiturient = new Abiturient();
        Faculty newFaculty = new Faculty();
        List<Entity> newRating = new ArrayList<>();
        idFaculty = Integer.parseInt(request.getParameter("faculty"));
        if (request.getParameter("idAbiturient") != null) {
            id = Integer.parseInt(request.getParameter("idAbiturient"));
            abiturient.setId(id);
        }
        User user = new User();
        user.setId((int) session.getAttribute("userId"));
        abiturient.setUser(user);
        abiturient.setSurname(request.getParameter("abiturientSurname"));
        abiturient.setName(request.getParameter("abiturientName"));
        abiturient.setPatronomic(request.getParameter("abiturientPatronomic"));
        abiturient.setDiplomRating(Float.parseFloat(request.getParameter("diplomRating")));
        newFaculty.setId(idFaculty);
        abiturient.setFaculty(newFaculty);
        for (int i = 1; i < 5; i++) {
            if (request.getParameter("sub" + i) != "" && request.getParameter("sub" + i) != null) {
                Rating rating = new Rating();
                Subjects subject = new Subjects();
                subject.setId(Integer.parseInt(request.getParameter("sub" + i)));
                rating.setSubjects(subject);
                rating.setMark(Integer.parseInt(request.getParameter("mark" + i)));
                newRating.add(rating);
                rating.setAbiturient(abiturient);
            }
        }
        abiturient.setRating(newRating);
        try {
            manager.getService(ServiceAbiturient.class).add(abiturient);
            abiturient = (Abiturient) manager.getService(ServiceAbiturient.class).findById(abiturient.getId()).get(0);
            request.setAttribute("abiturient", abiturient);
            request.setAttribute("save", "Сохранено!");
            LOG_INFO.info("Addition/update successful abiturient id = " + abiturient.getId() + ", name = "
                    + abiturient.toString() + " into the data store by user " + session.getAttribute("login"));
        } catch (Exception e) {
            request.setAttribute("save", "ERROR!");
            LOG_DB_ERROR.error("Error addition/update abiturient", e);
            LOG_INFO.error("Error addition/update abiturient");
        }
        return "/main?COMMAND=editAbiturient";
    }
}