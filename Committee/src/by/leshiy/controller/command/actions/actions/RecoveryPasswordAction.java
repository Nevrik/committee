package by.leshiy.controller.command.actions.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.User;
import by.leshiy.service.ServiceUser;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

/**
 * Functionality not completed!
 * 
 * @author Pavel Liashchou
 * 
 */
// TODO add functionality for recovery password
public class RecoveryPasswordAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (!request.getParameter("email").isEmpty()) {
            String pass = ((User) manager.getService(ServiceUser.class).findByLogin(
                    request.getParameter("email"))).getPassword();
            System.out.println(pass);
        } else {
            System.out.println("email is empty");
        }
        return "/WEB-INF/jsp/user/recoveryPassword.jsp";
    }

}
