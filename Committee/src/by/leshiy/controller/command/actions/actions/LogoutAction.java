package by.leshiy.controller.command.actions.actions;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.leshiy.controller.command.Action;

public class LogoutAction implements Action {

    @SuppressWarnings("finally")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = "/Committee/main?COMMAND=firstPage";
        HttpSession session = request.getSession();
        session.invalidate();
        try {
            response.sendRedirect(page);
        } catch (IOException e) {
            e.printStackTrace();
            return page;
        } finally {
            return page;
        }
    }
}