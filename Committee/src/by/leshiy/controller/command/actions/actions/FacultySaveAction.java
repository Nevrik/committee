package by.leshiy.controller.command.actions.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.leshiy.controller.command.Action;
import by.leshiy.db.Faculty;
import by.leshiy.db.Subjects;
import by.leshiy.service.ServiceFaculty;
import by.leshiy.service.ServiceSubjects;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class FacultySaveAction implements Action {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Faculty faculty = new Faculty();
        faculty.setName(request.getParameter("facultyName"));
        faculty.setRecruitPlan(Integer.parseInt(request.getParameter("facultyRecruitPlan")));
        faculty.setId(Integer.parseInt(request.getParameter("faculty")));
        List<Subjects> subject = new ArrayList<>();
        for (int i = 1, count = 5; i < count; i++) {
            Subjects sub = null;
            String s = Integer.toString(i);
            if (request.getParameter("select" + s) != null) {
                sub = (Subjects) manager.getService(ServiceSubjects.class)
                        .findById(Integer.parseInt(request.getParameter("select" + s))).get(0);
                subject.add(sub);
            }
        }
        faculty.setSubjects(new ArrayList<Subjects>(subject));
        try {
            manager.getService(ServiceFaculty.class).add(faculty);
            request.setAttribute("save", "Сохранено!");
            LOG_INFO.info("Update successful faculty id = " + faculty.getId() + ", name = " + faculty.getName()
                    + " into the data store.");
        } catch (Exception e) {
            request.setAttribute("save", "ERROR!");
            LOG_DB_ERROR.error("Error addition/update faculty.", e);
            LOG_INFO.error("Error addition/update faculty.");
        }
        return "/main?COMMAND=editFaculty";
    }
}
