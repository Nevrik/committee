package by.leshiy.controller.command.actions.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.leshiy.controller.command.Action;
import by.leshiy.service.ServiceAbiturient;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;
/**
 * Action changes the value of the field 'registrationMark' to the opposite
 * 
 * @author Pavel Liashchou
 * 
 */
public class ChangeRegistrationMarkAction implements Action {

    public static final Logger LOG_DB_ERROR = Logger.getLogger("dbError");
    public static final Logger LOG_INFO = Logger.getLogger("info");
    private ServiceManager manager = new ServiceManagerImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException,
            Exception {
        boolean newRegMark = false;
        boolean oldRegMark = Boolean.parseBoolean(request.getParameter("registrationMark"));
        if (!oldRegMark) {
            newRegMark = true;
        }
        String idAbiturient = request.getParameter("idAbiturient");
        try {
            manager.getService(ServiceAbiturient.class).setRegistrationMark(newRegMark, Integer.parseInt(idAbiturient));
            LOG_INFO.info("Update successful registration mark for abiturient id = " + idAbiturient);
        } catch (Exception e) {
            LOG_DB_ERROR.error("Error update registration mark for abiturient id = " + idAbiturient, e);
            LOG_INFO.error("Error update registration mark for abiturient id = " + idAbiturient);
        }
        return "/main?COMMAND=showAbitList";
    }
}
