package by.leshiy.controller.command.actions.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.leshiy.controller.command.Action;
import by.leshiy.db.User;
import by.leshiy.dbHelp.ToHashCode;
import by.leshiy.service.ServiceUser;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class NewUserSaveAction implements Action {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = new User();
        user.setPassword(ToHashCode.md5(request.getParameter("pass")));
        user.setLogin(request.getParameter("mail"));
        try {
            new ServiceManagerImpl().getService(ServiceUser.class).add(user);
        } catch (Exception e) {
            // TODO Add logger
            e.printStackTrace();
        }
        return "/main?COMMAND=loginAction";
    }
}
