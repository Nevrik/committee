package by.leshiy.controller.command.actions.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.leshiy.controller.command.Action;
import by.leshiy.db.User;
import by.leshiy.dbHelp.ToHashCode;
import by.leshiy.service.ServiceUser;
import by.leshiy.service.proxy.ServiceManager;
import by.leshiy.service.proxy.ServiceManagerImpl;

public class LoginAction implements Action {

    private ServiceManager manager = new ServiceManagerImpl();
    private User user;
    public static final Logger LOG_INFO = Logger.getLogger("info");

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String page = "/main?COMMAND=firstPage";
        String password = null;
        request.setAttribute("loginError", true);
        if (request.getParameter("mail") != null) {
            try {
                user = ((User) manager.getService(ServiceUser.class).findByLogin(
                        request.getParameter("mail").toString()));
                if (user.getPassword() != null) {
                    password = user.getPassword();

                    if (request.getParameter("pass") != null && password != null
                            && password.equals(ToHashCode.md5(request.getParameter("pass")))) {
                        session.setAttribute("login", user.getLogin());
                        session.setAttribute("role", user.getRole());
                        session.setAttribute("userId", user.getId());
                        request.setAttribute("loginError", false);
                        if (user.getRole().toString().equals("ABITURIENT")) {
                            page = "/main?COMMAND=showAbiturient";
                        }
                    } else {
                        // TODO add information for user
                        LOG_INFO.info("Logon attempt by incorrect password for login " + request.getParameter("mail")
                                + " from IP-adress " + request.getRemoteAddr());
                        System.out.println("Авторизация не пройдена!");
                    }
                } else {
                    throw new NullPointerException();
                }
            } catch (Exception e) {
                // TODO add information for user
                LOG_INFO.info("Logon attempt by incorrect login. " + request.getParameter("mail") + " from IP-adress "
                        + request.getRemoteAddr());
            }
        }
        return page;
    }
}