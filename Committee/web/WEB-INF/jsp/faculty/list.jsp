<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<table border="1">
	<tr>
		<th width="30" align="center">№</th>
		<th width="150" align="center">Наименование</th>
		<th align="center">План набора</th>
		<th align="center">Проходной балл</th>
		<th width="150" align="center">Предметы</th>
	</tr>
	<c:forEach var="faculty" items="${facultyList}">
		<tr>
			<td align="center">${faculty.id}</td>
			<td align="center">
				<form name="commands" action="main" method="post">
					<input type="hidden" name="COMMAND" value="editFaculty"> <input type="hidden" name="faculty"
						value="${faculty.id}"> <a href="" onclick="this.parentNode.submit(); return false;">${faculty.name}</a>
				</form>
			</td>
			<td align="center">${faculty.recruitPlan}</td>
			<td align="center">${faculty.checkRating}</td>
			<td colspan="3"><c:forEach var="subject" items="${faculty.subjects}">
				${subject.name}<br>
				</c:forEach></td>
		</tr>
	</c:forEach>
</table>