<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>

<link rel="stylesheet" type="text/css" href="css/div.css">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Приемная комиссия - УО "ВГУ им. П.М. Машерова"</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:import url="/WEB-INF/jsp/constParts/adminMenu.jsp"></c:import>
			<div id="content" align="center">
				<c:import url="/WEB-INF/jsp/faculty/list.jsp"></c:import>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>