<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/div.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Приемная комиссия - УО "ВГУ им. П.М. Машерова"</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:if test="${loginError == true}">
				<div id="loginError">Неверный логин или пароль!</div>
			</c:if>
			<c:choose>
				<c:when test="${role eq 'ABITURIENT'}">
					<c:import url="/WEB-INF/jsp/constParts/abiturientMenu.jsp"></c:import>
				</c:when>
				<c:when test="${role eq 'ADMINISTRATOR'}">
					<c:import url="/WEB-INF/jsp/constParts/adminMenu.jsp"></c:import>
				</c:when>
				<c:otherwise>
					<c:import url="/WEB-INF/jsp/constParts/loginMenu.jsp"></c:import>
				</c:otherwise>
			</c:choose>
			<div id="content" align="center">

				<!-- форма выбора факультета -->
				<p align="center">
					<c:forEach var="fac" items="${facultyList}">
						<c:set var="id" value="0" />
						<c:set var="accessingMark" />
						<c:set var="recruitPlan" value="${fac.recruitPlan}" />
						${fac.name}<br>
						План набора - <b>${fac.recruitPlan}</b>
						<br>
						<c:if test="${fac.name == fac.name}">
						Проходной балл  - <b>${fac.checkRating}</b>
						</c:if>
						<p>
						<table border="1">
							<tr>
								<th style="width: 30px" align="center">№</th>
								<th style="width: 200px" align="center">ФИО</th>
								<th style="width: 100px" align="center">Общий балл</th>
								<th style="width: 100px" align="center">Примечание</th>
							</tr>
							<c:forEach var="abit" items="${abiturientList}">
								<c:if test="${abit.faculty.id == fac.id}">
									<c:set var="recruitPlan" value="${recruitPlan-1}" />
									<c:set var="result" value="${abit.diplomRating}" />
									<c:set var="id" value="${id + 1}" />
									<c:if test="${recruitPlan<0}">
										<c:set var="color" value="lightgrey" />
									</c:if>
									<tr bgcolor="${color}">
										<td align="center">${id}</td>
										<td>${abit.surname}&nbsp${abit.name}&nbsp${abit.patronomic}</td>
										<td align="center"><c:forEach var="rating" items="${abit.rating}">
												<c:set var="result" value="${result+rating.mark}" />
											</c:forEach>${result}</td>
										<c:if test="${recruitPlan==0}">
											<c:set var="accessingMark" value="${result}" />
										</c:if>
										<td><c:if test="${result>=fac.checkRating and fac.checkRating>0}">
									Поступил.
								</c:if> <c:if test="${result<fac.checkRating or fac.checkRating==0}">
									Не поступил.
								</c:if></td>
									</tr>
									<c:set var="color" value="white" />
								</c:if>
							</c:forEach>
						</table>
						<hr>
					</c:forEach>
				</p>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>