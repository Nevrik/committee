<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="id" scope="page" value="0" />
<table border="1">
	<tr>
		<th width="30" align="center">№</th>
		<th align="center" width="250px">Наименование</th>
	</tr>
	<c:forEach var="sub" items="${subject}">
		<c:set var="selectId" scope="page" value="0" />
		<c:set var="id" value="${id+1}" />
		<tr>
			<td align="center">${id}</td>
			<td align="center">
				<form name="commands" action="main" method="post">
					<input type="hidden" name="COMMAND" value="editSubject"> <input type="hidden" name="idSubject"
						value="${sub.id}"> <a href="" onclick="this.parentNode.submit(); return false;">${sub.name}</a>
				</form>
			</td>
		</tr>
	</c:forEach>
</table>