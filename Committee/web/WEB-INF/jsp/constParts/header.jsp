<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="header" align="center">
	<form name="commands" action="main" method="post">
		<a href="" onclick="this.parentNode.submit(); return false;"><img src="img/epam-logo.jpg"
			height="150px" align="left"></a> <input type="hidden" name="COMMAND" value="firstPage">
	</form>
	<h1 align="center">
		Проект<br> "Приемная комиссия"
	</h1>
</div>
<div id="loginName" align="center">
	<c:if test="${not empty login}">
	Вы вошли в Систему. Ваш логин ${login}&nbsp;&nbsp;&nbsp;<a href="main?COMMAND=logoutAction">Выход</a>
	</c:if>
</div>