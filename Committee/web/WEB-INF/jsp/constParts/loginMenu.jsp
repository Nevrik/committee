<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="menu">
	<form id="loginForm" method="post" action="main">
		<fieldset>
			<legend>Вход</legend>
			<dl>
				<dt>Логин (e-mail)</dt>
				<dd>
					<INPUT name="mail" type="email" placeholder="введите e-mail" value="a@mail.ru">
				</dd>
				<dt>Пароль</dt>
				<dd>
					<INPUT name="pass" type="password" placeholder="введите пароль" value="admin">
				</dd>
				<dt></dt>
				<dd>
					<INPUT type="submit" value="Войти"> <input type="hidden" name="COMMAND"
						value="loginAction">
				</dd>
			</dl>
			<a href="main?COMMAND=recoveryPassword">Забыли пароль?</a> <br> <br> <a
				href="main?COMMAND=newUserBean">Регистрация нового абитуриента</a>
		</fieldset>
	</form>
	<c:import url="/WEB-INF/jsp/constParts/usersList.jsp"></c:import>
</div>