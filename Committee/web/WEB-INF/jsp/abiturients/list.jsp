<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- порядковый номер в таблице -->
<c:set var="id" scope="page" value="0" />

<!-- перезагрузка страницы при смене факультета -->
<script type="text/javascript">
	function submitForm(f) {
		f.submit();
	}
</script>

<!-- форма выбора факультета -->
<p align="left">
<form action="main" id="selectForm" method="post">
	<input type="hidden" name="COMMAND" value="showAbitList"> <select name="facultySelect"
		onchange="javascript:submitForm(this.form)">
		<option id="selectedF" value="${abiturientList[0].faculty.name}">${abiturientList[0].faculty.name}
			<c:forEach var="fac" items="${facultyList}">
				<option value="${fac.id}">${fac.name}
			</c:forEach>
	</select>
</form>
<p>
	<!-- таблица со списком абитуриентов -->
<table border="1">

	<!-- заголовки -->
	<tr>
		<th width="50px" align="center">№</th>
		<th align="center" width="150px">OK</th>
		<th align="center" width="250px">ФИО</th>
		<th align="center" width="200px">Факультет</th>
		<th align="center" width="100px">Балл аттестата</th>
		<th align="center" width="300px">Предмет</th>
		<th align="center" width="150px">Оценка</th>

	</tr>

	<!-- list of abiturients -->
	<c:forEach var="abit" items="${abiturientList}">
		<c:set var="selectId" scope="page" value="0" />
		<c:set var="id" value="${id+1}" />
		<!-- 		variable for table property 'rowspan' -->
		<c:set var="subCount" scope="page" value="${abit.rating.size()+1}" />
		<tr>
			<td rowspan="${subCount}" align="center">${id}</td>

			<!--	change the registration mark	-->
			<td rowspan="${subCount}" align="center">
				<form name="registration${id}" action="main" method="post">
					<input type="hidden" name="COMMAND" value="changeRegistrationMark"> 
					<input name="registrationMark" type="hidden" value="${abit.registrationMark}"> 
					<input name="idAbiturient" type="hidden" value="${abit.id}">
					<c:if test="${abit.registrationMark}">
						<input name="id" type="checkbox" value="${abit.id}"
							onchange="javascript:submitForm(this.form)" checked>
					</c:if>
					<c:if test="${!abit.registrationMark}">
						<input name="id" type="checkbox" value="${abit.id}"
							onchange="javascript:submitForm(this.form)">
					</c:if>
				</form>
			</td>

			<!-- 			link to the edit page of the Abiturient-->
			<td rowspan="${subCount}" align="center">
				<form name="commands" action="main" method="post">
					<input type="hidden" name="COMMAND" value="editAbiturient"> 
					<input type="hidden" name="faculty" value="${abit.faculty.id}"> 
					<input type="hidden" name="idAbiturient" value="${abit.id}"> 
					<a href="" onclick="this.parentNode.submit(); return false;">${abit.surname}
					<br>
						${abit.name} ${abit.patronomic}
					</a>
				</form>
			</td>
			<td rowspan="${subCount}">${abit.faculty.name}</td>
			<td rowspan="${subCount}">${abit.diplomRating}</td>

			<!-- 			subjects and marks -->
			<c:forEach var="r" items="${abit.rating}">
				<tr>
					<td>${r.subjects.name}</td>
					<td align="center">${r.mark}</td>
				</tr>
			</c:forEach>
		</tr>
	</c:forEach>
</table>