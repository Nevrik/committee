<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/div.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Новый пользователь</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>

		<div id="content" align="center" style="width: 1000px;
	height: 150px">
			<form action="main">
				Введите e-mail <input name="mail" style="width: 300px" type="email"
					placeholder="адрес электронной почты">
				<p>
					Введите пароль <input name="pass" style="width: 300px" type="password" placeholder="ваш пароль">
				<p>
					<input type="submit" value="Зарегистрироваться"> <input type="hidden" name="COMMAND"
						value="newUserSave">
			</form>
			<form action="index.jsp">
				<input type="submit" value="Назад">
			</form>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>