<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/div.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Восстановление пароля</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:import url="/WEB-INF/jsp/constParts/loginMenu.jsp"></c:import>
			<div id="content">
				<form action="main">
					Введите e-mail
					<p>
						<input name="email" style="width: 300px" type="email" placeholder="адрес электронной почты">
					<p>
						<input type="submit" value="Восстановить"> <input type="hidden" name="COMMAND"
							value="recoveryPassword">
				</form>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>