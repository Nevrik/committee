<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<c:set var="selectId" scope="page" value="0" />
<c:set var="id" scope="page" value="0" />
<html>
<script type="text/javascript">
	function selectChanged(idFac) {
		document.getElementById('faculty').value = idFac;
		document.forms['changes'].submit();
	}
</script>
<link rel="stylesheet" type="text/css" href="css/div.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Приемная комиссия - УО "ВГУ им. П.М. Машерова"</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:choose>
				<c:when test="${role=='ABITURIENT'}">
					<c:import url="/WEB-INF/jsp/constParts/abiturientMenu.jsp"></c:import>
				</c:when>
				<c:when test="${role=='ADMINISTRATOR'}">
					<c:import url="/WEB-INF/jsp/constParts/adminMenu.jsp"></c:import>
				</c:when>
				<c:otherwise>
					<c:import url="/WEB-INF/jsp/constParts/loginMenu.jsp"></c:import>
				</c:otherwise>
			</c:choose>
			<div id="content" align="center">
				<c:if test="${not empty save}">
					<div id="save">
						<c:out value="${save}" />
					</div>
				</c:if>
				<form id="changes" name="changes" action="main" method="post">
					<input type="hidden" name="COMMAND" value="editAbiturient"> <input type="hidden"
						name="faculty" id="faculty" value=""> <input type="hidden" name="idAbiturient"
						value="${abiturient.id}">
				</form>
				<form name="saveChanges" action="main" method="post">
					<input type="hidden" name="idAbiturient" value="${abiturient.id}">
					<table>
						<tr>
							<td width="100">Фамилия</td>
							<td width="200"><input name="abiturientSurname" style="width: 98%;" type="text"
								value="${abiturient.surname}" maxlength="20" required></td>
						</tr>
						<tr>
							<td>Имя</td>
							<td><input name="abiturientName" style="width: 98%;" type="text"
								value="${abiturient.name}" maxlength="20" required></td>
						</tr>
						<tr>
							<td>Отчество</td>
							<td><input name="abiturientPatronomic" style="width: 98%;" type="text"
								value="${abiturient.patronomic}" maxlength="20" required></td>
						</tr>
						<tr>
							<td>Балл диплома</td>
							<td><input name="diplomRating" style="width: 98%;" type="number" step="0.1" min="1"
								max="10" pattern="([1-9]{1}[,.]{0,1}[1-9]{0,1}|10$)" value="${abiturient.diplomRating}"
								title="Балл диплома - число от 1 до 10 с точностью до 0,1" required></td>
						</tr>
					</table>
					<p>
					<table>
						<tr>
							<td>Факультет</td>
							<td><SELECT name="faculty" style="width: 100%;
	margin: 5px 0px"
								onchange="javascript:selectChanged(this.value)">
									<OPTION VALUE="${faculty.id}">${faculty.name}
										<c:forEach var="fac" items="${facultyList}">
											<OPTION VALUE="${fac.id}">${fac.name}
										</c:forEach>
							</SELECT></td>
						</tr>
						<tr>
							<td width="150" rowspan="3">Оценки</td>
							<td width="350"><c:forEach var="subject" items="${faculty.subjects}">
									<c:set var="selectId" value="${selectId+1}" />
									<input type="hidden" name="sub${selectId}" value="${subject.id}">
									<c:set var="id" value="0" />
									<input type="text" VALUE="${subject.name}" readonly="readonly">
									<c:set var="subjectMark" value="0"></c:set>
									<c:forEach begin="0" end="${abiturient.rating.size()}">
										<c:if test="${subject.id == abiturient.rating[id].subjects.id}">
											<c:set var="subjectMark" value="${abiturient.rating[id].mark}"></c:set>
										</c:if>
										<c:set var="id" value="${id+1}" />
									</c:forEach>
									<input name="mark${selectId}" type="number" step="0,1" min="0" max="100"
										pattern="(0$|[1-9]{1,2}[0]{0,1}|100$)" VALUE="${subjectMark}"
										title="Оценка должна быть в пределах от 0 до 100">
								</c:forEach></td>
						</tr>
					</table>
					<p>
						<input style="width: 100px" type="reset" value="Сбросить"> <input type="hidden"
							name="COMMAND" value="saveAbiturient"> <input style="width: 100px" type="submit"
							value="Сохранить" onmouseup="javascript:select(this.form)">
				</form>
			</div>
			<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>

		</div>
	</div>
</body>
</html>