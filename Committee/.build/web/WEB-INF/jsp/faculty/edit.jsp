<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<c:set var="selectId" scope="page" value="0" />
<html>
<link rel="stylesheet" type="text/css" href="css/div.css">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Приемная комиссия - УО "ВГУ им. П.М. Машерова"</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:import url="/WEB-INF/jsp/constParts/adminMenu.jsp"></c:import>
			<div id="content" align="center">
				<c:if test="${not empty save}">
					<div id="save">
						<c:out value="${save}" />
					</div>
				</c:if>
				<form name="fac" method="post">
					<input type="hidden" name="idFaculty" value="${faculty.id}">
					<table>
						<tr>
							<td width="150">Наименование</td>
							<td><input name="facultyName" style="width: 98%;" type="text" value="${faculty.name}"
								required></td>
						</tr>
						<tr>
							<td>План приема</td>
							<td><input name="facultyRecruitPlan" style="width: 98%;" type="number"
								value="${faculty.recruitPlan}" required></td>
						</tr>
						<tr>
							<td width="150" rowspan="3">Экзаменационные предметы</td>
							<td width="350"><c:forEach var="subject" items="${faculty.subjects}">
									<c:set var="selectId" value="${selectId+1}" />
									<SELECT name="select${selectId}" style="width: 100%;
	margin: 5px 0px">
										<OPTION VALUE="${subject.id}" selected>${subject.name}
											<c:forEach var="sub" items="${subjectList}">
												<OPTION VALUE="${sub.id}">${sub.name}
											</c:forEach>
									</SELECT>
								</c:forEach></td>
						</tr>
					</table>
					<input style="width: 100px" type="reset" value="Сбросить"> <input type="hidden"
						name="COMMAND" value="saveFaculty"> <input style="width: 100px" type="submit"
						value="Сохранить" onmouseup="javascript:selectChanged(this.form)" />
				</form>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>