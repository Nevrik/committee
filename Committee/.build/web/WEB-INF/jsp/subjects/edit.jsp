<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/div.css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Приемная комиссия - УО "ВГУ им. П.М. Машерова"</title>
</head>
<body id="bod">
	<div id="main">
		<c:import url="/WEB-INF/jsp/constParts/header.jsp"></c:import>
		<div id="columns">
			<c:import url="/WEB-INF/jsp/constParts/adminMenu.jsp"></c:import>
			<div id="content" align="center">
				<c:if test="${not empty save}">
					<div id="save">
						<c:out value="${save}" />
					</div>
				</c:if>
				<form name="fac" method="post">
					<table>
						<tr>
							<td width="150" rowspan="3">Наименование</td>
							<td width="350"><input name="editingSubject" style="width: 98%;" type="text"
								value="${subject.name}"></td>
						</tr>
					</table>
					<input style="width: 100px" type="reset" value="Сбросить"> <input type="hidden"
						name="COMMAND" value="saveSubject"> <input type="hidden" name="idSubject"
						value="${subject.id}"> <input style="width: 100px" type="submit" value="Сохранить" />
				</form>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/constParts/footer.jsp"></c:import>
	</div>
</body>
</html>