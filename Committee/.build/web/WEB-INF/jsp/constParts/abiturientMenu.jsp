<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- information for display takes from AbiturientShowAction -->

<c:set var="abiturient" value="${abiturient}"/>
<div id="menu">
	<form action="main">
		<p>
			ФИО <br> ${abiturient.surname} ${abiturient.name} ${abiturient.patronomic}
		<hr>
		Факультет<br> ${abiturient.faculty.name}<br>
		<hr>
		Общий балл ${result}
		<hr>
		Балл аттестата<br> ${abiturient.diplomRating}
		<hr>
		Оценки <br>
		<table style="border: 0px">
			<c:forEach var="rating" items="${abiturient.rating}">
				<tr>
					<td style="border: 0px" width="150">${rating.subjects.name}</td>
					<td style="border: 0px" width="50" align="center">${rating.mark}</td>
				</tr>
			</c:forEach>
		</table>
		<p>
			<c:if test="${abiturient.registrationMark eq false or abiturient.registrationMark == null}">
				<input type="submit" value="Редактировать">
			</c:if>
			<c:choose>
				<c:when test="${abiturient.id == null}">
					<input type="hidden" name="COMMAND" value="newAbiturient">
				</c:when>
				<c:when test="${abiturient.id != null}">
					<input type="hidden" name="COMMAND" value="editAbiturient">
					<input type="hidden" name="idAbiturient" value="${abiturient.id}">
					<input type="hidden" name="faculty" value="${abiturient.faculty.id}">
				</c:when>
			</c:choose>
	</form>
	<c:import url="/WEB-INF/jsp/constParts/usersList.jsp"></c:import>
</div>